import requests
import json
from events.keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    try:
        lat = json.loads(response.content)[0]["lat"]
        lon = json.loads(response.content)[0]["lon"]
    except IndexError:
        return None

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    try:
        description = json.loads(response.content)["weather"][0]["description"]
        temperature = json.loads(response.content)["main"]["temp"]
        weather = {"temperature": temperature, "description": description}
    except IndexError:
        return None
    return weather


# def get_weather_data(city, state):
#     params = {
#         "q": f"{city},{state},US",
#         "limit": 1,
#         "appid": OPEN_WEATHER_API_KEY,
#     }
#     url = "http://api.openweathermap.org/geo/1.0/direct"
#     response = requests.get(url, params=params)
#     content = json.loads(response.content)

#     try:
#         latitude = content[0]["lat"]
#         longitude = content[0]["lon"]
#     except (KeyError, IndexError):
#         return None

#     params = {
#         "lat": latitude,
#         "lon": longitude,
#         "appid": OPEN_WEATHER_API_KEY,
#         "units": "imperial",
#     }
#     url = "https://api.openweathermap.org/data/2.5/weather"
#     response = requests.get(url, params=params)
#     content = json.loads(response.content)

#     try:
#         return {
#             "description": content["weather"][0]["description"],
#             "temp": content["main"]["temp"],
#         }
#     except (KeyError, IndexError):
#         return None
